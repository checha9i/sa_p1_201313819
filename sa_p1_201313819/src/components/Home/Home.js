import React from 'react';
import './Home.css';
import { Container, Row, Col, Jumbotron } from 'reactstrap';
import Contacts from '../Contacts/Contacts';

const Home = () => (
  <div className="Home">
    <Container>
      <Jumbotron>
        <h1 className="display-3">Practica 1</h1>
        <p className="lead">
          Cesar Javier Solares Orozco
        </p>
        <hr className="my-2" />
        <p>201313819
        </p>
      </Jumbotron>
      <Row>
        <Col>
          <Contacts />
        </Col>
      </Row>
    </Container>
  </div>
);


export default Home;
