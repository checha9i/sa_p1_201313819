import React, { Component } from 'react';

import './Contacts.css';
import {
  Row, Col, Table, Button, Container
} from 'reactstrap';
import axios from 'axios';


class Contacts extends Component {

  constructor(props) {
    super(props);

    this.state = {
      contacts: []
    }

    this.create = this.create.bind(this);
  }


  componentDidMount() {
    this.interval = setInterval(() => {
      axios.get('https://api.softwareavanzado.world/index.php?webserviceClient=administrator&webserviceVersion=1.0.0&option=contact&api=hal&filter[search]=201313819')
        .then(response => {
          console.log(response.data._embedded.item)
          this.setState({
            contacts: response.data._embedded.item
          });
        });
    }, 1000)
  }



  componentWillUnmount() {
    clearInterval(this.interval);
  }

  create(e) {
    for (let i = 1; i <= 10; i++) {
      const formData = new FormData()
      formData.append('name', '201313819_' + i)
      axios.post('https://api.softwareavanzado.world/index.php?webserviceClient=administrator&webserviceVersion=1.0.0&option=contact&api=hal', formData, {})
        .then(response => {
          console.log('Contacto creado.');
        });
    }
    alert('Contactos creados.');
  }

  render() {

    const ListAll = ({ contacts }) => {
      return (
        <Table striped responsive>
          <thead>
            <tr>
              <th>ID</th>
              <th>NAME</th>
            </tr>
          </thead>
          <tbody>
            {contacts.map((contact, key) => {
              return (
                <tr key={key}>
                  <th scope="row">{contact.id}</th>
                  <td>{contact.name}</td>
                </tr>
              )
            })}
          </tbody>
        </Table>
      )
    }

    return (
      <Container>
        <Row>
          <Col>
            <Button outline onClick={this.create}>
              <span className="fa fa-plus fa-lg"></span> Create Contacts
              </Button>
          </Col>
        </Row>
        <Row>
          <Col>
            <ListAll contacts={this.state.contacts} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Contacts;
